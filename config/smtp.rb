SMTP_SETTINGS = {
  address: 'http://localhost:1025', # example: "smtp.sendgrid.net"
  authentication: :plain,
  domain: 'hot.dev', # example: "heroku.com"
  enable_starttls_auto: true,
  password: '',
  port: "587",
  user_name: ''
}

if ENV["EMAIL_RECIPIENTS"].present?
  Mail.register_interceptor RecipientInterceptor.new(ENV["EMAIL_RECIPIENTS"])
end
